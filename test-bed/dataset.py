import sys
import json
import settings

def generate(s):
    """
    Generate the test data set from the given list of dictionaries
    Create the power set and write the json representation in the
    data set file.
    """
    print("Generating the test data set in %s" % settings.DATA_SET)
    minimum, maximum, total = settings.MAX_PAYLOAD_SIZE, -1, 0
    x = len(s)
    with open(settings.DATA_SET, "w") as data_set:
        for i in range(1, 1 << x):
            obj = {"data": [s[j] for j in range(x) if i & (1 << j)]}
            encoded = json.dumps(obj, separators=(',', ':'))
            data_set.write("%s\n" % encoded)
            current_size = settings.utf8len(encoded) + 1
            minimum = min(current_size, minimum)
            maximum = max(current_size, maximum)
            total += current_size
            if current_size > settings.MAX_PAYLOAD_SIZE:
                print("%d: %d Byte; Warning, size exceeded!" %
                    (i, current_size))
    print("Done.")
    n = (1 << x) - 1
    print("Number of payloads: %d" % n)
    print("Smallest payload: %d Byte" % minimum)
    print("Largest payload: %d Byte" % maximum)
    print("Average payload: %d Byte" % ((float)(total) / (float)(n)))
    print("Total dataset size: %d Byte" % total)


def get_sensor_list(base_size=-1):
    sensor_list = [
        {"sensor": "ID456", "type": "temp", "value": 50},
        {"sensor": "ID007", "type": "humidity", "value": 78},
        {"sensor": "ID100", "type": "light", "value": 109},
        {"sensor": "ID861", "type": "sound", "value": 30},
        {"sensor": "ID456", "type": "pressure", "value": 8},
        {"sensor": "ID71623", "type": "radiation", "value": 12361},
        {"sensor": "IDXX122", "type": "airquality", "value": "bad"},
        {"sensor": "1YYAS", "type": "temp", "value": 14},
        {"sensor": "HUAS1", "type": "light", "value": 77},
        {"sensor": "ID15VA", "type": "distance", "value": 971341},
    ]
    if base_size == -1 or base_size > len(sensor_list):
        return sensor_list
    else:
        return sensor_list[:base_size]

def simple_generator(x_size, n):
    if x_size <= 0:
        raise Exception("Invalid packet size: %d" % x_size)

    x = "A" * (x_size - 1)
    with open(settings.DATA_SET, "w") as data_set:
        for i in range(0, n):
            data_set.write("%s\n" % x)
    print("Generated %d Byte long packages %d times" % (x_size, n))

if __name__ == "__main__":
    # generate(get_sensor_list(int(sys.argv[1])))
    simple_generator(int(sys.argv[1]), int(sys.argv[2]))
