#!/bin/bash

# Create the project's python 2.7 virutal environment if it does not already exist

VENV_DIR="./venv"

set -e

if [ ! -d  $VENV_DIR ]; then
    echo "Python virtual environment not found. Creating..."
    which virtualenv &> /dev/null
    if [ $? -ne 0 ]; then
        echo "virtualenv not installed on the system. Exiting..." 1>&2
        exit 2
    fi
    virtualenv --python=python2 $VENV_DIR
    echo "Use source ${VENV_DIR}/bin/activate to activate the virtual environment"
    echo "Use deactivate to exit the virtual environment"
fi

set +e
