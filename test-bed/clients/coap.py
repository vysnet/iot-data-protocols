from __future__ import print_function
import os
import sys
sys.path.insert(0, os.path.dirname(__file__))
import random
import time
import string
import settings
from coapthon import defines
from coapthon.client.helperclient import HelperClient

def start(confirmable=False):
    if not confirmable:
        print("Started CoAP client with NONCON requests")
    else:
        print("Started CoAP client with CON request")

    confirmation = "NON" if not confirmable else "CON"
    address = (settings.SERVER_ADDRESS, settings.COAP_SERVER_PORT)
    client = HelperClient(server=address)
    print("Connected to server! Dispatching started.")
    try:
        data_set = open(settings.DATA_SET, "r")
        counter = 1
        for payload in data_set:
            request = client.mk_request(defines.Codes.POST, settings.COAP_SERVER_PATH)
            request.token = ''.join(random.choice(string.ascii_letters) for _ in range(2))
            request.payload = payload
            request.type = defines.Types.get(confirmation)
            print("Sending packet #%d (%d Bytes)..." % (counter, settings.utf8len(payload)), end=" ")
            try:
                response = client.send_request(request, timeout=10)
            except Exception as _:
                print("Lost response!")
            else:
                if response is not None and response.code != defines.Codes.VALID.number:
                    print("Not expected response code")
                    print(response.pretty_print())
                if response is not None:
                    print("Sent!")
                else:
                    print("Lost response!")
            counter += 1
            time.sleep(1)
    finally:
        data_set.close()
        client.stop()
    print("Done!")
