import os
import sys
sys.path.insert(0, os.path.dirname(__file__))

import settings
from coapthon import defines
from coapthon.server.coap import CoAP
from coapthon.resources.resource import Resource

received_data = 0
received_packets = 0

class PiggybackedResponse(Resource):
    def __init__(self, confirmable, name="PiggybackedStorage"):
        super(PiggybackedResponse, self).__init__(name)
        self.confirmable = confirmable

    def render_POST_advanced(self, request, response):
        global received_data, received_packets
        received_packets += 1
        received_data += settings.utf8len(request.payload)
        # print("Received %s" % request.payload)
        if self.confirmable is None:
            response.type = defines.Types.get("ACK")
        elif self.confirmable is False:
            response.type = defines.Types.get("NON")
        elif self.confirmable:
            response.type = defines.Types.get("CON")
        response.code = defines.Codes.VALID.number
        return self, response

class SeparateResponse(Resource):
    def __init__(self, confirmable, name="SeparateStorage"):
        super(SeparateResponse, self).__init__(name)
        self.confirmable = confirmable

    def render_POST_advanced(self, request, response):
        return self, response, self.render_POST_separate

    def render_POST_separate(self, request, response):
        global received_packets, received_data
        received_packets += 1
        received_data += settings.utf8len(request.payload)
        # print("Received %s" % request.payload)
        if not self.confirmable:
            response.type = defines.Types.get("NON")
        else:
            response.type = defines.Types.get("CON")
        response.code = defines.Codes.VALID.number
        return self, response

class PiggybackedServer(CoAP):
    def __init__(self, confirmable):
        CoAP.__init__(self, ("0.0.0.0", settings.COAP_SERVER_PORT))
        self.add_resource(
            settings.COAP_SERVER_PATH,
            PiggybackedResponse(confirmable)
        )

class SeparateServer(CoAP):
    def __init__(self, confirmable):
        CoAP.__init__(self, ("0.0.0.0", settings.COAP_SERVER_PORT))
        self.add_resource(
            settings.COAP_SERVER_PATH,
            SeparateResponse(confirmable)
        )

def start(piggybacked=True, confirmable=True):
    global received_data, received_packets
    if piggybacked:
        server = PiggybackedServer(confirmable)
        print("Piggybacked CoAP server started!")
    else:
        server = SeparateServer(confirmable)
        print("Separate CoAP server started!")
    try:
        print("Listening on port %d" % settings.COAP_SERVER_PORT)
        server.listen()
    except KeyboardInterrupt:
        print("Server stopped.")
        print("Received %d Byte" % received_data)
        print("From %d Packages at total" % received_packets)
        server.close()
