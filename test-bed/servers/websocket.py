import os
import sys
sys.path.insert(0, os.path.dirname(__file__))

import settings
from SimpleWebSocketServer import WebSocket
from SimpleWebSocketServer import SimpleWebSocketServer

received_data = 0
received_packages = 0
class Connection(WebSocket):
    def handleMessage(self):
        global received_data, received_packages
        current_size = settings.utf8len(self.data)
        received_data += current_size
        received_packages += 1
        # print("Received", self.data)
        # print("Size: %d Byte\n" % current_size)

    def handleConnected(self):
        print(self.address, "connected")

    def handleClose(self):
        global received_data, received_packages
        print("Total received %d Byte (%d Packages)" %
              (received_data, received_packages)
        )
        received_data = 0
        received_packages = 0
        print(self.address, "disconnected")


def start():
    server = SimpleWebSocketServer(
        "",
        settings.WEBSOCKET_SERVER_PORT,
        Connection
    )
    s = "WebSocket server started on port %d" % settings.WEBSOCKET_SERVER_PORT
    s += "\nUse Ctr+C to stop it."
    print(s)
    server.serveforever()
