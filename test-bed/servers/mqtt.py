import os
import sys
sys.path.insert(0, os.path.dirname(__file__))
import shutil
import settings
import subprocess

def start():
    # build the server configuration file
    conf_file = os.path.join(settings.PROJECT_DIR, "servers", "mosquitto.conf")
    example_file = os.path.join(settings.PROJECT_DIR, "servers", "mosquitto.conf.example")
    if os.path.exists(conf_file):
        os.remove(conf_file)
    shutil.copy2(example_file, conf_file)

    try:
        subprocess.call(["mosquitto", "-c", conf_file, "-p", str(settings.MQTT_SERVER_PORT)])
    except KeyboardInterrupt:
        pass
