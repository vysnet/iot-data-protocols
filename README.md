# IoT Data Protocols Comparison

A paper that theoretically and experimentally compares the
overhead of MQTT, CoAP and WebSocket protocols,
primarily for IoT/constrained networks use cases.

## Table of Contents
- [Description](#description)
- [Paper](#paper)
- [Talk](#talk)
- [Test bed](#test-bed)

### Description
This repository contains a paper for a qualitative and quantitative comparison of the overhead
of CoAP, MQTT and WebSocket protocols. The comparison assumes constrained nodes in IoT use cases
but provides general insides about the protocols' overhead.
The work was conducted as part of a seminar, 
organized by the [Lab for Network Architectures and Services](https://net.in.tum.de) at [TUM](https://www.tum.de/).
It consists of:
- the [paper](#paper)
- a [talk](#talk) that was given to present the main results.
- a [test bed](#test-bed) that was used to perform the quantitiative analysis.

Download the repository with:
```
git clone https://gitlab.com/v45k0/iot-data-protocols.git
```

### Paper
The latest compiled version can be found [here](https://gitlab.com/v45k0/iot-data-protocols/blob/master/paper/seminar.pdf).

**Compiling** from source code:

**Prerequisites**: A LaTeX distribution (e.g. TeXLive), `pdflatex`, `bibtex` to compile the LaTeX code and `inkscape` to compile the svg graphics.
All LaTeX packages that are needed to compile from source code should be present in the repositories of your LaTeX distribution.
However, it's possible that some of them are not installed by default and thus the compilation fails. Installing all missing
LaTeX dependencies should solve such an issue.
```
cd paper
make clean
make
```

### Talk
The latest compiled version of the presentation slides can be found [here](https://gitlab.com/v45k0/iot-data-protocols/raw/master/talk/slides.pdf).

**Compiling** from source code:

**Prerequisites**: A LaTeX distribution (e.g. TeXLive), `pdflatex`, `bibtex` to compile the LaTeX code and `inkscape` to compile the svg graphics.
All LaTeX packages that are needed to compile from source code should be present in the repositories of your LaTeX distribution.
However, it's possible that some of them are not installed by default and thus the compilation fails. Installing all missing
LaTeX dependencies should solve such an issue.
```
cd talk
make clean
make
```

### Test Bed
The following instructions describe how to execute the testbed and
should be run from the `test-bed` folder. For more information about the
configuration, please refer to the [paper](#paper).

**Prerequisites**: `python2.7`,
[`python virtualenv`](https://virtualenv.pypa.io/en/stable/) (globally executable),
[`mosquitto`](https://mosquitto.org/download/) (globally executable),
`GNU Make`.

The software packages that are going to be automatically installed
in the virtual python environment (see below for compiling the project)
and are used for the client/server components can be found
in the `requirements.txt` file.

Any nix system should be supported out of the box (Linux, BSD, OSX).
Windows is possible but not tested.

**Compiling** the testbed and generating the
dataset for benchmarking from source with:
```
make dependencies
make dataset PACKET_SIZE=128 PACKET_COUNT=100
make
```
Tweak the `PACKET_SIZE` and `PACKET_COUNT` variables
passed to the `make dataset` routine if you want
to use a different dataset pool.

To reproduce the results, compile the test bed on two machines -
one for the client and one for the server
(or simulate a LAN with a network simulator so that all networking
layers can be present when sending packages from the client
to the server). Set the IP of the server in the `settings.py` file
on the client side.
Then start the appropriate
configuration on each side (server before client)
based on the protocol you want to benchmark (see below).

If you want to simulate packet loss, check [netem](https://wiki.linuxfoundation.org/networking/netem).
You can also check [this quick SO guide](https://stackoverflow.com/a/615757/3046584).

#### WebSocket
```
./testbed server --category websocket
./testbed client --category websocket_single
```

##### CoAP
- Non-cofirmable requests, non-confirmable responses:
```
./testbed server --category coap_piggybacked_nonconfirmable
./testbed client --category coap_nonconfirmable
```

- Non-confirmable requests, confirmable responses:
```
./testbed server --category coap_piggybacked_confirmable
./testbed client --category coap_nonconfirmable
```

- Confirmable requests, piggybacked responses:
```
./testbed server --category coap_piggybacked_ack
./testbed client --category coap_confirmable
```

- Confirmable requests, separate non-confirmable responses:
```
./testbed server --category coap_separated_nonconfirmable
./testbed client --category coap_confirmable
```

- Confirmable requests, separate confirmable responses:
```
./testbed server --category coap_separated_confirmable
./testbed client --category coap_confirmable
```

##### MQTT
- QoS 0 publishing without authentication:
```
./testbed server --category mqtt
./testbed client --category mqtt_qos_0
```

- QoS 1 publishing without authentication:
```
./testbed server --category mqtt
./testbed client --category mqtt_qos_1
```

- QoS 2 publishing without authentication:
```
./testbed server --category mqtt
./testbed client --category mqtt_qos_2
```
